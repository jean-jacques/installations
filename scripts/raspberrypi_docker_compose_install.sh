#!/bin/bash

# INSTALL DOCKER-COMPOSE FOR RASPBERRY PI
sudo apt update -y
sudo apt-get install libffi-dev libssl-dev
sudo apt install python3-dev
sudo apt-get install -y python3 python3-pip
docker-compose --version