#!/bin/bash

## INSTALL KUBERNETES FOR DEBIAN 11
sudo apt -y install apt-transport-https gnupg2 curl
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
echo "deb http://apt.kubernetes.io/ kubernetes-xenial main" | tee -a /etc/apt/sources.list.d/kubernetes.list
sudo apt update -y
sudo apt -y install kubeadm kubelet kubectl

sudo systemctl enable kubelet