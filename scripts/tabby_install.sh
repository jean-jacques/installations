#!/bin/bash

cd /tmp
wget https://github.com/Eugeny/tabby/releases/download/v1.0.197/tabby-1.0.197-linux-x64.deb
sudo dpkg -i tabby-1.0.197-linux-x64.deb
rm -rf ./tabby-1.0.197-linux-x64.deb
cd -

cp ../files/config.yaml ~/.config/tabby