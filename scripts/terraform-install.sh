#!/bin/bash

# INSTALL TERRAFORM FOR DEBIAN 11
sudo apt update -y
curl -sSL httcurl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
sudo apt-add-repository "deb [arch=$(dpkg --print-architecture)] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
sudo apt update
sudo apt install terraform