#!/bin/bash

# INSTALL PROGRAMS
if command -v "codium","docker","docker-compose","vagrant","terraform" >/dev/null 2>&1; then
  echo "tous les programmes sont à jour"
else
  printf "\n== Installing programs...\n"
sudo apt update
./vscodium_install.sh
./docker_install.sh
./docker_compose.sh
./vagrant_install.sh
./terraform_install.sh
fi