
#!/bin/bash

# UPDATE
echo "\n
██╗   ██╗██████╗ ██████╗  █████╗ ████████╗███████╗
██║   ██║██╔══██╗██╔══██╗██╔══██╗╚══██╔══╝██╔════╝
██║   ██║██████╔╝██║  ██║███████║   ██║   █████╗  
██║   ██║██╔═══╝ ██║  ██║██╔══██║   ██║   ██╔══╝  
╚██████╔╝██║     ██████╔╝██║  ██║   ██║   ███████╗
 ╚═════╝ ╚═╝     ╚═════╝ ╚═╝  ╚═╝   ╚═╝   ╚══════╝"
sudo apt-get update

echo "\n
██╗   ██╗██████╗  ██████╗ ██████╗  █████╗ ██████╗ ███████╗
██║   ██║██╔══██╗██╔════╝ ██╔══██╗██╔══██╗██╔══██╗██╔════╝
██║   ██║██████╔╝██║  ███╗██████╔╝███████║██║  ██║█████╗  
██║   ██║██╔═══╝ ██║   ██║██╔══██╗██╔══██║██║  ██║██╔══╝  
╚██████╔╝██║     ╚██████╔╝██║  ██║██║  ██║██████╔╝███████╗
 ╚═════╝ ╚═╝      ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═════╝ ╚══════╝"
sudo apt-get upgrade

# paquets basiques
sudo apt-get install git wget curl net-tools nmap htop python3-pip zip openssh-server gimp wireguard -y

# VSCODIUM
echo "\n
██╗   ██╗███████╗ ██████╗ ██████╗ ██████╗ ██╗██╗   ██╗███╗   ███╗    ██╗███╗   ██╗███████╗████████╗ █████╗ ██╗     ██╗     
██║   ██║██╔════╝██╔════╝██╔═══██╗██╔══██╗██║██║   ██║████╗ ████║    ██║████╗  ██║██╔════╝╚══██╔══╝██╔══██╗██║     ██║     
██║   ██║███████╗██║     ██║   ██║██║  ██║██║██║   ██║██╔████╔██║    ██║██╔██╗ ██║███████╗   ██║   ███████║██║     ██║     
╚██╗ ██╔╝╚════██║██║     ██║   ██║██║  ██║██║██║   ██║██║╚██╔╝██║    ██║██║╚██╗██║╚════██║   ██║   ██╔══██║██║     ██║     
 ╚████╔╝ ███████║╚██████╗╚██████╔╝██████╔╝██║╚██████╔╝██║ ╚═╝ ██║    ██║██║ ╚████║███████║   ██║   ██║  ██║███████╗███████╗
  ╚═══╝  ╚══════╝ ╚═════╝ ╚═════╝ ╚═════╝ ╚═╝ ╚═════╝ ╚═╝     ╚═╝    ╚═╝╚═╝  ╚═══╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚══════╝╚══════╝"
./scripts/vscodium-install.sh

echo "\n
████████╗ █████╗ ██████╗ ██████╗ ██╗   ██╗
╚══██╔══╝██╔══██╗██╔══██╗██╔══██╗╚██╗ ██╔╝
   ██║   ███████║██████╔╝██████╔╝ ╚████╔╝ 
   ██║   ██╔══██║██╔══██╗██╔══██╗  ╚██╔╝  
   ██║   ██║  ██║██████╔╝██████╔╝   ██║   
   ╚═╝   ╚═╝  ╚═╝╚═════╝ ╚═════╝    ╚═╝"
./scripts/tabby_install.sh

# DOCKER
# echo "/n
# ██████╗  ██████╗  ██████╗██╗  ██╗███████╗██████╗     ██╗███╗   ██╗███████╗████████╗ █████╗ ██╗     ██╗     
# ██╔══██╗██╔═══██╗██╔════╝██║ ██╔╝██╔════╝██╔══██╗    ██║████╗  ██║██╔════╝╚══██╔══╝██╔══██╗██║     ██║     
# ██║  ██║██║   ██║██║     █████╔╝ █████╗  ██████╔╝    ██║██╔██╗ ██║███████╗   ██║   ███████║██║     ██║     
# ██║  ██║██║   ██║██║     ██╔═██╗ ██╔══╝  ██╔══██╗    ██║██║╚██╗██║╚════██║   ██║   ██╔══██║██║     ██║     
# ██████╔╝╚██████╔╝╚██████╗██║  ██╗███████╗██║  ██║    ██║██║ ╚████║███████║   ██║   ██║  ██║███████╗███████╗
# ╚═════╝  ╚═════╝  ╚═════╝╚═╝  ╚═╝╚══════╝╚═╝  ╚═╝    ╚═╝╚═╝  ╚═══╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚══════╝╚══════╝"
# ./scripts/docker-install.sh

# CMATRIX SCREENSAVER
# echo "\n
#  ██████╗███╗   ███╗ █████╗ ████████╗██████╗ ██╗██╗  ██╗    ██╗███╗   ██╗███████╗████████╗ █████╗ ██╗     ██╗     
# ██╔════╝████╗ ████║██╔══██╗╚══██╔══╝██╔══██╗██║╚██╗██╔╝    ██║████╗  ██║██╔════╝╚══██╔══╝██╔══██╗██║     ██║     
# ██║     ██╔████╔██║███████║   ██║   ██████╔╝██║ ╚███╔╝     ██║██╔██╗ ██║███████╗   ██║   ███████║██║     ██║     
# ██║     ██║╚██╔╝██║██╔══██║   ██║   ██╔══██╗██║ ██╔██╗     ██║██║╚██╗██║╚════██║   ██║   ██╔══██║██║     ██║     
# ╚██████╗██║ ╚═╝ ██║██║  ██║   ██║   ██║  ██║██║██╔╝ ██╗    ██║██║ ╚████║███████║   ██║   ██║  ██║███████╗███████╗
#  ╚═════╝╚═╝     ╚═╝╚═╝  ╚═╝   ╚═╝   ╚═╝  ╚═╝╚═╝╚═╝  ╚═╝    ╚═╝╚═╝  ╚═══╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚══════╝╚══════╝"
# ./scripts/cmatrix-install.sh

# VIRTUALBOX
echo "\n
██╗   ██╗██╗██████╗ ████████╗██╗   ██╗ █████╗ ██╗     ██████╗  ██████╗ ██╗  ██╗
██║   ██║██║██╔══██╗╚══██╔══╝██║   ██║██╔══██╗██║     ██╔══██╗██╔═══██╗╚██╗██╔╝
██║   ██║██║██████╔╝   ██║   ██║   ██║███████║██║     ██████╔╝██║   ██║ ╚███╔╝ 
╚██╗ ██╔╝██║██╔══██╗   ██║   ██║   ██║██╔══██║██║     ██╔══██╗██║   ██║ ██╔██╗ 
 ╚████╔╝ ██║██║  ██║   ██║   ╚██████╔╝██║  ██║███████╗██████╔╝╚██████╔╝██╔╝ ██╗
  ╚═══╝  ╚═╝╚═╝  ╚═╝   ╚═╝    ╚═════╝ ╚═╝  ╚═╝╚══════╝╚═════╝  ╚═════╝ ╚═╝  ╚═╝"
./scripts/virtualbox-install.sh

# VAGRANT
# echo "\n
# ██╗   ██╗ █████╗  ██████╗ ██████╗  █████╗ ███╗   ██╗████████╗
# ██║   ██║██╔══██╗██╔════╝ ██╔══██╗██╔══██╗████╗  ██║╚══██╔══╝
# ██║   ██║███████║██║  ███╗██████╔╝███████║██╔██╗ ██║   ██║   
# ╚██╗ ██╔╝██╔══██║██║   ██║██╔══██╗██╔══██║██║╚██╗██║   ██║   
#  ╚████╔╝ ██║  ██║╚██████╔╝██║  ██║██║  ██║██║ ╚████║   ██║   
#   ╚═══╝  ╚═╝  ╚═╝ ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═══╝   ╚═╝"
# ./scripts/vagrant-install.sh

# # JENKINS INSTALL
# echo "\n
#      ██╗███████╗███╗   ██╗██╗  ██╗██╗███╗   ██╗███████╗    ██╗███╗   ██╗███████╗████████╗ █████╗ ██╗     ██╗     
#      ██║██╔════╝████╗  ██║██║ ██╔╝██║████╗  ██║██╔════╝    ██║████╗  ██║██╔════╝╚══██╔══╝██╔══██╗██║     ██║     
#      ██║█████╗  ██╔██╗ ██║█████╔╝ ██║██╔██╗ ██║███████╗    ██║██╔██╗ ██║███████╗   ██║   ███████║██║     ██║     
# ██   ██║██╔══╝  ██║╚██╗██║██╔═██╗ ██║██║╚██╗██║╚════██║    ██║██║╚██╗██║╚════██║   ██║   ██╔══██║██║     ██║     
# ╚█████╔╝███████╗██║ ╚████║██║  ██╗██║██║ ╚████║███████║    ██║██║ ╚████║███████║   ██║   ██║  ██║███████╗███████╗
#  ╚════╝ ╚══════╝╚═╝  ╚═══╝╚═╝  ╚═╝╚═╝╚═╝  ╚═══╝╚══════╝    ╚═╝╚═╝  ╚═══╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚══════╝╚══════╝"
# ./scripts/jenkins-install.sh

# # KUBERNETES INSTALL
# echo "\n
# ██╗  ██╗ █████╗ ███████╗    ██╗███╗   ██╗███████╗████████╗ █████╗ ██╗     ██╗     
# ██║ ██╔╝██╔══██╗██╔════╝    ██║████╗  ██║██╔════╝╚══██╔══╝██╔══██╗██║     ██║     
# █████╔╝ ╚█████╔╝███████╗    ██║██╔██╗ ██║███████╗   ██║   ███████║██║     ██║     
# ██╔═██╗ ██╔══██╗╚════██║    ██║██║╚██╗██║╚════██║   ██║   ██╔══██║██║     ██║     
# ██║  ██╗╚█████╔╝███████║    ██║██║ ╚████║███████║   ██║   ██║  ██║███████╗███████╗
# ╚═╝  ╚═╝ ╚════╝ ╚══════╝    ╚═╝╚═╝  ╚═══╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚══════╝╚══════╝"
# ./scripts/kubernetes-install.sh

echo "\n
██████╗  ██████╗ ███╗   ██╗███████╗██╗
██╔══██╗██╔═══██╗████╗  ██║██╔════╝██║
██║  ██║██║   ██║██╔██╗ ██║█████╗  ██║
██║  ██║██║   ██║██║╚██╗██║██╔══╝  ╚═╝
██████╔╝╚██████╔╝██║ ╚████║███████╗██╗
╚═════╝  ╚═════╝ ╚═╝  ╚═══╝╚══════╝╚═╝"